USE `warehouse_db` ;

SELECT * FROM type_accounts;
SELECT * FROM accounts;

insert into type_accounts(id,name) values (1,"Admin");
insert into type_accounts values (2,"Director");
insert into type_accounts values (3,"Manager");
insert into type_accounts values (4,"Employee Import");
insert into type_accounts values (5,"Employee Adjustment");
insert into type_accounts values (6,"Employee Export");

insert into accounts values (1,"Nguyen Thanh Dat","chayngaydi@gmail.com","admin01",now(),0945135184,"nam","246 Le Loi",0,1);
insert into accounts values (2,"Faker","t1faker@gmail.com","direc01",now(),0945135185,"nam","247 Le Loi",0,2);
insert into accounts values (3,"Neymar Jr","barcancl@gmail.com","manag01",now(),0945135143,"nam","132 Le Loi",0,3);
insert into accounts values (4,"LeeJiEun","leejieun@gmail.com","emplo01",now(),0945432384,"nu","321 Le Loi",0,4);
insert into accounts values (5,"Ronaldo","ronaldo@gmail.com","emplo02",now(),0945567184,"nam","246 Le Loi",0,5);
insert into accounts values (6,"Nguyen Thanh Tung","sontungmtp@gmail.com","emplo03",now(),0945635184,"nam","246 Le Loi",0,6);

insert into contacts values(1, "2023-12-1", "Hop dong 2 nam", 1, 2,0);
select * from contacts;
-- select * from contacts where  expiration > 2020-10-23 and idDirector = (select idDirector from stores as st, accounts as ac, manager_detail as md  
-- where acc.id = 3 and ac.id = md.idManger and md.idStore = st.id group by idDirector)

-- select * from contacts where expiration > 2020-10-23 and idDirector = (select idDirector from stores as st, accounts as ac, manager_detail as md  where acc.id = 3 and ac.id = md.idManger and md.idStore = st.id group by idDirector)

select * from contacts 
where 
expiration > 2020-10-23 and 
idDirector = (select idDirector from stores as st, accounts as ac, manager_detail as md  
								where ac.id = 3 and ac.id = md.idManger and md.idStore = st.id 
                                group by idDirector);
