package Models;

import java.util.Date;
import java.util.List;

import Controlers.ContactControlers;
import Controlers.DirectorControlers;
import Entities.ContactEntities;
import Entities.DirectorEntities;
import Entities.TypeAccountEntities;

public class AdminModels {
	
	public boolean createDirector(String name, Date birthday, String address, int phone, String gender, String email, String password) {
		DirectorEntities director = new DirectorEntities();
		director.setName(name);
		director.setBirthday(birthday);
		director.setAddress(address);
		director.setPhone(phone);
		director.setGender(gender);
		director.setEmail(email);
		director.setPassword(password);
		TypeAccountModels typeAccountModels = new TypeAccountModels();
		TypeAccountEntities typeAccount = typeAccountModels.findOne("DIRECTOR");
		director.setIdTypeAccount(typeAccount.getId());
		DirectorControlers directorControler = new DirectorControlers();
		boolean result = directorControler.inster(director);
		if(result) return true;
		return false;
	}
	
	public boolean updateDirector(int id,String name, Date birthday, String address, int phone, String gender, String email, String password) {
		DirectorEntities director = new DirectorEntities();
		director.setId(id);
		director.setName(name);
		director.setBirthday(birthday);
		director.setAddress(address);
		director.setPhone(phone);
		director.setGender(gender);
		director.setEmail(email);
		director.setPassword(password);
		TypeAccountModels typeAccountModels = new TypeAccountModels();
		TypeAccountEntities typeAccount = typeAccountModels.findOne("DIRECTOR");
		director.setIdTypeAccount(typeAccount.getId());
		DirectorControlers directorControler = new DirectorControlers();
		boolean result = directorControler.update(director);
		if(result) return true;
		return false;
	}
	
	public boolean deleteDirector(int id) {
		DirectorEntities director = new DirectorEntities();
		director.setId(id);
		DirectorControlers directorControler = new DirectorControlers();
		boolean result = directorControler.delete(director);
		if(result) return true;
		return false;
	}
	
	public boolean createContact(int idAdmin, int idDirector, Date expiration, String note) {
		ContactModels contactModels = new ContactModels();
		DirectorModels directorModels = new DirectorModels();
		
		DirectorEntities director = directorModels.findOne(idDirector);
		if(director != null) {
			boolean rs = contactModels.insert(idAdmin, idDirector, expiration, note);
			return rs;
		}else {
			return false;
		}
	}

	public boolean updateContact(int id, String note) {
		ContactModels contactModels = new ContactModels();
		boolean rs = contactModels.updateContact(id, note);
		return rs;
	}

	public boolean deleteContact(int id) {
		ContactControlers contactControlers = new ContactControlers();
		boolean rs = contactControlers.deleteContact(id);
		return rs;
	}

	public List<ContactEntities> getAllContact() {
		ContactControlers contactControlers = new ContactControlers();
		List<ContactEntities> list = contactControlers.getAll();
		return list;
	}

	public List<DirectorEntities> getAllDirector() {
		DirectorControlers directorControler = new DirectorControlers();
		List<DirectorEntities> list = directorControler.getAll();
		return list;
	}

	public boolean contractExtension(int idContract, Date dateExtension) {
		ContactModels contactModels = new ContactModels();
		boolean rs = contactModels.updateContractExtension(idContract, dateExtension);
		return rs;
	}

}
