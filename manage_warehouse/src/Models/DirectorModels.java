package Models;

import java.util.Date;
import java.util.List;

import Controlers.DirectorControlers;
import Controlers.ProductCategoriesControlers;
import Controlers.ProductControlers;
import Controlers.RecordControlers;
import Controlers.StoreControlers;
import Entities.AccountEntities;
import Entities.DirectorEntities;
import Entities.ManagerEntities;
import Entities.ProductCategoriesEntities;
import Entities.ProductEntities;
import Entities.RecordDetailEntities;
import Entities.RecordEntities;
import Entities.StoreEntities;
import Entities.TypeAccountEntities;

public class DirectorModels {

	public List<DirectorEntities> getAll() {
		DirectorControlers directorControler = new DirectorControlers();
		List<DirectorEntities> list = directorControler.getAll();
		return list;
	}

	public DirectorEntities findOne(int idDirect) {
		DirectorControlers contactControlers = new DirectorControlers();
		DirectorEntities contact = contactControlers.findOne(idDirect);
		return contact;
	}

	public boolean insertStore(String name, String address, int phone) {
		try {
			StoreEntities storeEntities = new StoreEntities();
			storeEntities.setName(name);
			storeEntities.setAddress(address);
			storeEntities.setPhone(phone);
			storeEntities.setCreateAt(new Date());
			StoreControlers storeControler = new StoreControlers();
			boolean rs = storeControler.insert(storeEntities);
			return rs;
		} catch (Exception err) {
			return false;
		}
	}

	public boolean updateStore(int id, String name, String address, int phone) {
		try {
			StoreEntities storeEntities = new StoreEntities();
			storeEntities.setName(name);
			storeEntities.setAddress(address);
			storeEntities.setPhone(phone);
			storeEntities.setId(id);
			StoreControlers storeControler = new StoreControlers();
			boolean rs = storeControler.update(storeEntities);
			return rs;
		} catch (Exception err) {
			return false;
		}
	}

	public boolean deleteStore(int id) {
		try {
			StoreEntities storeEntities = new StoreEntities();
			storeEntities.setId(id);
			StoreControlers storeControler = new StoreControlers();
			boolean rs = storeControler.delete(storeEntities);
			return rs;
		} catch (Exception err) {
			return false;
		}
	}

	public boolean insertProductCategories(String name, String description, int idStore) {
		ProductCategoriesControlers pcControler = new ProductCategoriesControlers();
		ProductCategoriesEntities pcEntities = new ProductCategoriesEntities();
		pcEntities.setName(name);
		pcEntities.setDescription(description);
		boolean rsPC = pcControler.insertPc(pcEntities);
		if (rsPC) {
			ProductCategoriesEntities pcEntitiesNew = pcControler.findOne(pcEntities);
			if (pcEntitiesNew != null || !String.valueOf(pcEntities.getId()).isEmpty()) {
				boolean rs = pcControler.insertPCDetail(pcEntitiesNew.getId(), idStore);
				return rs;
			} else
				return false;
		} else
			return false;
	}

	public boolean updateProductCategories(int idPCDetail, String name, String description) {
		ProductCategoriesControlers pcControler = new ProductCategoriesControlers();
		ProductCategoriesEntities pcEntities = pcControler.findOnePCDetail(idPCDetail);
		if (pcEntities != null) {
			boolean rs = pcControler.updateProducCategories(name, description, pcEntities.getId());
			return rs;
		} else
			return false;
	}

	public boolean deleteProductCategorieorForDirector(int idPCDetail) {
		ProductCategoriesControlers pcControler = new ProductCategoriesControlers();
		ProductCategoriesEntities pcEntities = pcControler.findOnePCDetail(idPCDetail);
		if (pcEntities != null) {
			boolean rs = pcControler.deleteProducCategories(pcEntities.getId());
			return rs;
		} else
			return false;
	}

	public boolean HandleProductCategorieorForStore(int idPCDetail, boolean isDelete) {
		ProductCategoriesControlers pcControler = new ProductCategoriesControlers();
		boolean rs = pcControler.handleProducCategories(idPCDetail, isDelete);
		return rs;
	}

	public List<ProductCategoriesEntities> getAllProductCatetories() {
		ProductCategoriesControlers pcControler = new ProductCategoriesControlers();
		List<ProductCategoriesEntities> list = pcControler.getAllProductCatetories();
		return list;
	}

	public boolean insertProduct(String name, String description, int prices, int cost, Date expiration,
			int idProductCategories) {
		ProductControlers productControler = new ProductControlers();
		ProductEntities product = new ProductEntities();
		product.setName(name);
		product.setDescription(description);
		product.setPrices(prices);
		product.setCost(cost);
		product.setExpiration(expiration);
		boolean rsProduct = productControler.insertProduct(product);
		if (rsProduct) {
			ProductEntities productNew = productControler.findOne(product);
			if (productNew != null) {
				System.out.println(productNew.getId());
				boolean rs = productControler.insertProductDetail(productNew.getId(), idProductCategories);
				return rs;
			} else
				return false;
		} else
			return false;
	}

	public boolean updateProduct(int id, String name, String description, int prices, int cost, Date expiration) {
		ProductControlers productControler = new ProductControlers();
		ProductEntities product = new ProductEntities();
		product.setId(id);
		product.setName(name);
		product.setDescription(description);
		product.setPrices(prices);
		product.setCost(cost);
		product.setExpiration(expiration);
		boolean rs = productControler.updateProduct(product);
		return rs;
	}

	public boolean deleteProduct(int id) {
		ProductControlers productControler = new ProductControlers();
		boolean rs = productControler.deleteProduct(id);
		return rs;
	}

	public boolean handleProduct(int idProductDetail, boolean isDelete) {
		ProductControlers productControler = new ProductControlers();
		boolean rs = productControler.handleProduct(idProductDetail, isDelete);
		return rs;
	}

	public List<ProductEntities> getAllProduct(int idDirector) {
		ProductControlers productControler = new ProductControlers();
		List<ProductEntities> list = productControler.getAllProduct(idDirector);
		return list;
	}

	public boolean insertManager(String name, Date birthday, String address, int phone, String gender, String email,
			String password, int idStore, int idDirector) {
		ManagerEntities manager = new ManagerEntities();
		manager.setName(name);
		manager.setBirthday(birthday);
		manager.setAddress(address);
		manager.setPhone(phone);
		manager.setGender(gender);
		manager.setEmail(email);
		manager.setPassword(password);
		TypeAccountModels typeAccountModels = new TypeAccountModels();
		StoreControlers storeControler = new StoreControlers();
		boolean rsStore = storeControler.findOne(idStore, idDirector);
		if (rsStore) {
			TypeAccountEntities typeAccount = typeAccountModels.findOne("MANAGER");
			manager.setIdTypeAccount(typeAccount.getId());
			DirectorControlers directorControler = new DirectorControlers();
			boolean result = directorControler.inster(manager);
			if (result)
				return true;
			return false;
		} else
			return false;
	}
	
	public boolean updateManager(int idManager,String name, Date birthday, String address, int phone, String gender, String email,
			String password, int idStore, int idDirector) {
		ManagerEntities manager = new ManagerEntities();
		manager.setId(idManager);
		manager.setName(name);
		manager.setBirthday(birthday);
		manager.setAddress(address);
		manager.setPhone(phone);
		manager.setGender(gender);
		manager.setEmail(email);
		manager.setPassword(password);
		TypeAccountModels typeAccountModels = new TypeAccountModels();
		StoreControlers storeControler = new StoreControlers();
		boolean rsStore = storeControler.findOne(idStore, idDirector);
		if (rsStore) {
			TypeAccountEntities typeAccount = typeAccountModels.findOne("MANAGER");
			manager.setIdTypeAccount(typeAccount.getId());
			DirectorControlers directorControler = new DirectorControlers();
			boolean result = directorControler.update(manager);
			if (result)
				return true;
			return false;
		} else
			return false;

	}

	public boolean deleteManager(int idManager, int idStore, int idDirector) {
		ManagerEntities manager = new ManagerEntities();
		manager.setId(idManager);
		TypeAccountModels typeAccountModels = new TypeAccountModels();
		StoreControlers storeControler = new StoreControlers();
		boolean rsStore = storeControler.findOne(idStore, idDirector);
		if (rsStore) {
			TypeAccountEntities typeAccount = typeAccountModels.findOne("MANAGER");
			manager.setIdTypeAccount(typeAccount.getId());
			DirectorControlers directorControler = new DirectorControlers();
			boolean result = directorControler.delete(manager);
			if (result)
				return true;
			return false;
		} else
			return false;

	}

	public List<AccountEntities> getAllManager(int idStore, int idDirector) {
		DirectorControlers directorControler = new DirectorControlers();
		List<AccountEntities> list = directorControler.getAllManager(idStore, idDirector);
		return list;
	}

	public List<RecordEntities> getAllRecord() {
		RecordControlers recordControler = new RecordControlers();
		List<RecordEntities> list = recordControler.getAll();
		return list;	
	}
	
	public boolean createRecord(String description, int createBy, int idStore, int idTypeRecords) {
		RecordControlers recordController = new RecordControlers();
		RecordEntities record = new RecordEntities();
		record.setDescription(description);
		record.setCreateBy(createBy);
		record.setIdStore(idStore);
		record.setIdTypeRecord(idTypeRecords);
		boolean rs = recordController.createRecord(record);
		return rs;
	}
	
	public boolean createRecordDetail(int idRecord, int idProduct, int number)
	{
		RecordControlers recordController = new RecordControlers();
		RecordDetailEntities recordDetail = new RecordDetailEntities();
		recordDetail.setIdRecord(idRecord);
		recordDetail.setIdProduct(idProduct);
		recordDetail.setNumber(number);
		boolean rs = recordController.createRecordDetail(recordDetail);
		return rs;
	}
	
	public RecordEntities getRecordById(int idRecord)
	{
		RecordEntities record = new RecordEntities();
		RecordControlers recordController = new RecordControlers();
		record = recordController.getRecord(idRecord);
		return record;
	}

	public RecordDetailEntities getDetailRecordById(int idDetailRecord)
	{
		RecordDetailEntities record = new RecordDetailEntities();
		RecordControlers recordController = new RecordControlers();
		record = recordController.getDetailRecordById(idDetailRecord);
		return record;
	}
	
	public boolean updateDetailRecord(int idRecord, int idDetailRecord, int idProduct, int number) {
		RecordControlers recordController = new RecordControlers();
		RecordDetailEntities recordDetail = new RecordDetailEntities();
		recordDetail.setId(idDetailRecord);
		recordDetail.setIdProduct(idProduct);
		recordDetail.setIdRecord(idRecord);
		recordDetail.setNumber(number);
		boolean rs = recordController.updateDetailRecord(recordDetail);
		return rs;
	}
	
	public boolean verifyRecord(int idRecord, int verifyBy) {
		RecordControlers recordController = new RecordControlers();
		boolean rs = recordController.verifyRecord(idRecord, verifyBy);
		return rs;
	}
}
