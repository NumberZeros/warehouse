package Models;

import java.util.Date;

import Controlers.AccountControlers;
import Entities.AccountEntities;
import Entities.TypeAccountEntities;

public class AccountModels {
	public static String ADMIN = "";
	public static String DIRECTOR = "";
	public static String EMPLOYEE_IMPORT = "Employee Import";
	public static String EMPLOYEE_ADJUSTMENT = "Employee Adjustment";
	public static String EMPLOYEE_EXPORT = "Employee Export";
	
	public AccountEntities handleLogin(String email, String password) {
		try {
			AccountEntities account = new AccountEntities();
			account.setEmail(email);
			account.setPassword(password);			
			AccountControlers accountControler = new AccountControlers();
			AccountEntities dataAccount = accountControler.handleLogin(account);
			TypeAccountModels typeModels= new TypeAccountModels();
			TypeAccountEntities dataType = typeModels.findOne(dataAccount.getIdTypeAccount());		
			ContactModels contactModels = new ContactModels();
			switch(dataType.getName()) {
			case "Admin": 
				return dataAccount;
			case "Directors": 
				boolean rsDirect = contactModels.getExpirationForDirector(dataAccount.getId());
				if(rsDirect) return dataAccount;
				else return null;
			case "Manager":
				boolean rsManager = contactModels.getExpirationForManager(dataAccount.getId());
				if(rsManager) return dataAccount;
				else return null;
			default: 
				boolean rsEmployee = contactModels.getExpirationForEmployee(dataAccount.getId());
				if(rsEmployee) return dataAccount;
				else return null;
			}
		}catch(Exception err) {
			return null;	
		}
	}
	
	public AccountEntities getInfo(AccountEntities account) {
		try {
			AccountControlers accountControler = new AccountControlers();
			AccountEntities dataAccount = accountControler.getInfo(account);
			return dataAccount;
		}catch(Exception err) {
			return null;	
		}
	}

	public boolean updateInfo(int id, String name, String email, String password, Date birthday, int phone, String gender,
			String address) {
		AccountEntities account = new AccountEntities();
		account.setId(id);
		account.setName(name);
		account.setEmail(email);
		account.setPassword(password);
		account.setBirthday(birthday);
		account.setPhone(phone);
		account.setAddress(address);
		AccountControlers accountControler = new AccountControlers();
		boolean rs = accountControler.updateInfo(account);
		return rs;
	}
}
