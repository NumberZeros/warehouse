package Models;

import java.util.List;

import Controlers.TypeAccountControlers;
import Entities.TypeAccountEntities;

public class TypeAccountModels {
	public List<TypeAccountEntities> getAll() {
		try {
			TypeAccountControlers typeAccountControler = new TypeAccountControlers();
			List<TypeAccountEntities> dataAccount = typeAccountControler.getAll();
			return dataAccount;
		}catch(Exception err) {
			return null;	
		}
	}

	public TypeAccountEntities findOne(Integer id) {
		TypeAccountControlers typeAccountControler = new TypeAccountControlers();
		if(!id.toString().isEmpty()) {
			TypeAccountEntities typeAccount = typeAccountControler.findOne(id);
			return typeAccount;
		}
		return null;
	}

	public TypeAccountEntities findOne(String name) {
		TypeAccountControlers typeAccountControler = new TypeAccountControlers();
		TypeAccountEntities typeAccount = typeAccountControler.findOne(name);
		return typeAccount;
	}
}
