package Models;

import java.util.Date;

import Controlers.ContactControlers;
import Entities.ContactEntities;

public class ContactModels {

	public ContactEntities findOne(int idContact) {
		ContactControlers contactControlers = new ContactControlers();
		ContactEntities contact = contactControlers.findOne(idContact);
		return contact;
	}

	public boolean insert(int idAdmin, int idDirector, Date expiration, String note) {
		ContactControlers contactControlers = new ContactControlers();
		boolean rs = contactControlers.insert(idAdmin, idDirector, expiration, note);
		return rs;
	}

	public boolean updateContact(int id, String note) {
		ContactControlers contactControlers = new ContactControlers();
		boolean rs = contactControlers.update(id, note);
		return rs;
	}

	public boolean getExpirationForDirector(int idDirector) {
		ContactControlers contactControlers = new ContactControlers();
		boolean rs = contactControlers.getExpirationForDirector(idDirector);
		return rs;
	}

	public boolean getExpirationForManager(int idManager) {
		ContactControlers contactControlers = new ContactControlers();
		boolean rs = contactControlers.getExpirationForManager(idManager);
		return rs;
	}

	public boolean getExpirationForEmployee(int idEmployee) {
		ContactControlers contactControlers = new ContactControlers();
		boolean rs = contactControlers.getExpirationForEmployee(idEmployee);
		return rs;
	}

	public boolean updateContractExtension(int idContract, Date dateExtension) {
		ContactControlers contactControlers = new ContactControlers();
		boolean rs = contactControlers.updateContractExtension(idContract, dateExtension);
		return rs;
	}

}
