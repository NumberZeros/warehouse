package Controlers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import Entities.RecordDetailEntities;
import Entities.RecordEntities;

public class RecordControlers {
	public boolean createRecord(RecordEntities record) {
		try {
			String sqlDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			String query = "insert into record(description, createBy, createAt, idStore, idTypeRecord) values ("
					+ "'"+record.getDescription()+"'" 
					+","+record.getCreateBy()
					+",'"+sqlDate +"'"
					+","+record.getIdStore()
					+","+record.getIdTypeRecord()
					+")";
			boolean result = HandleDB.Mutation(query);
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean createRecordDetail(RecordDetailEntities recordDetail) {
		try {
			ResultSet cost = (ResultSet) HandleDB.Query("select cost from products "
					+ "where id = "+recordDetail.getIdProduct());
			boolean result = false;
			//get cost from product success
			while (cost.next())
			{
				String query = "insert into record_detail(idRecord, idProduct, number, totalAmount) values ("
						+recordDetail.getIdRecord()
						+","+recordDetail.getIdProduct()
						+","+recordDetail.getNumber()
						+","+recordDetail.getNumber()*cost.getFloat("cost")
						+");";
				result = HandleDB.Mutation(query);
				//add a detailRecord success then update totalProduct and totalPrices of record
				if (result)
				{
					result = updateRecord(recordDetail.getIdRecord());
				}
			}
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public RecordEntities getRecord(int idRecord) {
		try {
			ResultSet data = (ResultSet) HandleDB.Query("select * from record where id = " + idRecord);	
			RecordEntities re = new RecordEntities();
			while(data.next()) {
				re.setId(data.getInt("id"));
				re.setCreateAt(data.getDate("createAt"));
				re.setCreateBy(data.getInt("createBy"));
				re.setDescription(data.getString("description"));
				re.setDelete(data.getBoolean("isDelete"));
				re.setIdStore(data.getInt("idStore"));
				re.setIdTypeRecord(data.getInt("idTypeRecord"));
				re.setTotalProducts(data.getInt("totalProducts"));
				re.setTotalPrices(data.getFloat("totalPrices"));
				re.setVerify(data.getBoolean("isVerify"));
				re.setVerifyBy(data.getInt("verifyBy"));
			}
			return re; 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<RecordEntities> getAll() {
		try {
			ResultSet data = (ResultSet) HandleDB.Query(
					"select * from record");
			List<RecordEntities> list = new ArrayList<>();
			while(data.next())
			{
				RecordEntities record = new RecordEntities();
				record.setId(data.getInt("id"));
				record.setCreateAt(data.getDate("createAt"));
				record.setCreateBy(data.getInt("createBy"));
				record.setDescription(data.getString("description"));
				record.setDelete(data.getBoolean("isDelete"));
				record.setIdStore(data.getInt("idStore"));
				record.setIdTypeRecord(data.getInt("idTypeRecord"));
				record.setTotalProducts(data.getInt("totalProducts"));
				record.setTotalPrices(data.getFloat("totalPrices"));
				record.setVerify(data.getBoolean("isVerify"));
				record.setVerifyBy(data.getInt("verifyBy"));
				list.add(record);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public RecordDetailEntities getDetailRecordById(int idDetailRecord) {
		try {
			RecordDetailEntities dataRecord = new RecordDetailEntities();
			ResultSet data = (ResultSet) HandleDB.Query("select * from record_detail where id = " + idDetailRecord);
			while (data.next())
			{
				dataRecord.setId(data.getInt("id"));
				dataRecord.setIdProduct(data.getInt("idProduct"));
				dataRecord.setIdRecord(data.getInt("idRecord"));
				dataRecord.setNumber(data.getInt("number"));
				dataRecord.setTotalAmount(data.getFloat("totalAmount"));
			}
			return dataRecord;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean updateDetailRecord(RecordDetailEntities recordDetail) {
		try {
			ResultSet cost = (ResultSet) HandleDB.Query("select cost from products where id = " + recordDetail.getIdProduct());
			boolean result = false;
			while (cost.next())
			{
				String query = "update record_detail set "
						+ "idProduct =" +recordDetail.getIdProduct()
						+ ", number = " +recordDetail.getNumber()
						+ ", totalAmount = " + recordDetail.getNumber()*cost.getFloat("cost")
						+" where id= "+ recordDetail.getId();
				result = HandleDB.Mutation(query);
				if (result)
				{
					result = updateRecord(recordDetail.getIdRecord());
				}
			}
			return result;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean updateRecord(int idRecord)
	{
		try {
			//select all record_detail 
			ResultSet detailRecord = (ResultSet) HandleDB.Query("select * from record_detail "
					+ "where idRecord = "+ idRecord + ";");
			float newTotalPrices = 0;
			int newTotalProducts = 0;
			boolean result = false;
			while (detailRecord.next())
			{
				newTotalPrices += detailRecord.getFloat("totalAmount");
				newTotalProducts += detailRecord.getInt("number");
			}
			//update record
			if (newTotalPrices != 0 && newTotalProducts != 0)
			{
				String queryUpdate = "update record set "
						+ "totalPrices = " + newTotalPrices
						+ ", totalProducts = " + newTotalProducts
						+ " where id = " + idRecord;
				result = HandleDB.Mutation(queryUpdate);
			}
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean verifyRecord(int idRecord, int verifyBy) {
		try {
			String query = "update record set "
					+ "isVerify = " + true
					+ ", verifyBy = " + verifyBy
					+ " where id = " + idRecord;
			boolean result = HandleDB.Mutation(query);
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}

