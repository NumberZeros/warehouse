package Controlers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import Entities.AccountEntities;

public class AccountControlers {
	public AccountEntities handleLogin(AccountEntities account) {
		try {
			ResultSet data = (ResultSet) HandleDB.Query("select * from accounts "
					+ "where"
					+ " email ='"+account.getEmail()+ "'"
					+ " and password='"+ account.getPassword()+"'"
					+ " and isDelete=" +account.getIsDelete()+";");
			AccountEntities dataAccount = new AccountEntities();
			while(data.next()) {
				dataAccount.setId(data.getInt("id")); ;
				dataAccount.setName(data.getString("name"));
				dataAccount.setAddress(data.getString("address"));
				dataAccount.setPhone(data.getInt("phone"));;
				dataAccount.setEmail(data.getString("email"));;
				dataAccount.setPassword(data.getString("password"));
				dataAccount.setGender(data.getString("gender"));
				dataAccount.setIdTypeAccount(data.getInt("idTypeAccount"));
				dataAccount.setBirthday(data.getDate("birthday"));
                return dataAccount;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	
	public AccountEntities getInfo(AccountEntities account) {
		try {
			ResultSet data = (ResultSet) HandleDB.Query("select * from accounts where id ='"+account.getId()+"'");
			AccountEntities dataAccount = new AccountEntities();
			while(data.next()) {
				dataAccount.setId(data.getInt("id")); ;
				dataAccount.setName(data.getString("name"));
				dataAccount.setAddress(data.getString("adress"));
				dataAccount.setPhone(data.getInt("phone"));;
				dataAccount.setEmail(data.getString("email"));;
				dataAccount.setPassword(data.getString("password"));
				dataAccount.setGender(data.getString("gender"));
				dataAccount.setIdTypeAccount(data.getInt("idTypeAccount"));
				dataAccount.setBirthday(data.getDate("birthday"));
                return dataAccount;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public boolean updateInfo(AccountEntities account) {
		try {
			String sqlDate = new SimpleDateFormat("yyyy-MM-dd").format(account.getBirthday());
			boolean rs = HandleDB.Mutation("update accounts set"
					+ " name ='"+ account.getName() +"'"
					+ ", email='" + account.getEmail() +"'"
					+ ", password ='"+ account.getPassword()+"'"
					+ ", birthday = '"+ sqlDate +"'"
					+ ", phone = " + account.getPhone()
					+ ", gender='"+ account.getGender() +"'"
					+ ", address='"+ account.getAddress()+ "'"
					+ " where id="+ account.getId());
		 return rs;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}
}
