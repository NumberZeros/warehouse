package Controlers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import Entities.ContactEntities;

public class ContactControlers {

	public ContactEntities findOne(int idContact) {
		try {
			ResultSet data = (ResultSet) HandleDB.Query("select * from contacts "
					+ "where id ='"+idContact+ "'");
			ContactEntities contact = new ContactEntities();
			while(data.next()) {
				contact.setId(data.getInt("id"));
				contact.setExpiration(data.getDate("expiration"));
				contact.setNote(data.getString("note"));
				contact.setIdAdmin(data.getInt("idAdmin"));
				contact.setIdDirector(data.getInt("idDirector"));
                return contact;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public boolean insert(int idAdmin, int idDirector, Date expiration, String note) {
		try {
			String sqlDate = new SimpleDateFormat("yyyy-MM-dd").format(expiration);
			String query = "insert into contacts(note,idAdmin, idDirector, expiration ) values("
					+ "'"+note+"'" 
					+",'"+idAdmin+"'"
					+",'"+idDirector+"'"
					+",'"+sqlDate+"'"
					+")";
			boolean result = HandleDB.Mutation(query);
			return result;
		}catch(Exception err) {
			return false;
		}
	}

	public boolean update(int id, String note) {
		try {
			String query = "update contacts set"
					+ " note = '"+note+"'" 
					+" where id='"+id+"'";
			boolean result = HandleDB.Mutation(query);
			return result;
		}catch(Exception err) {
			return false;
		}
	}

	public boolean deleteContact(int id) {
		try {
			String query = "update contacts set"
					+ " isDelete = "+true+"" 
					+" where id='"+id+"'";
			boolean result = HandleDB.Mutation(query);
			return result;
		}catch(Exception err) {
			return false;
		}
	}

	public List<ContactEntities> getAll() {
		try {
			ResultSet data = (ResultSet) HandleDB.Query("select * from contacts "
					+ "where isDelete ="+false+ "");
			List<ContactEntities> list = new ArrayList<>();
			while(data.next()) {
				ContactEntities contact = new ContactEntities();
				contact.setId(data.getInt("id")); ;
				contact.setExpiration(data.getDate("expiration"));
				contact.setNote(data.getString("note"));
				contact.setIdAdmin(data.getInt("idAdmin"));
				contact.setIdDirector(data.getInt("idDirector"));
                list.add(contact);
			}
			return list;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public boolean getExpirationForDirector(int idDirector) {
		try {
			String sqlDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			ResultSet data = (ResultSet) HandleDB.Query("select * from contacts "
					+ "where"
					+ " idDirector ="+idDirector+ ""
					+ " and expiration >"+sqlDate+ ""
					);
			ContactEntities contact = new ContactEntities();
			while(data.next()) {
				contact.setId(data.getInt("id")); ;
				contact.setExpiration(data.getDate("expiration"));
				contact.setNote(data.getString("note"));
				contact.setIdAdmin(data.getInt("idAdmin"));
				contact.setIdDirector(data.getInt("idDirector"));
                return true;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public boolean getExpirationForManager(int idManager) {
		try {
			String sqlDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			ResultSet data = (ResultSet) HandleDB.Query("select * from contacts"
					+ " where expiration > "+ sqlDate
					+ " and"
					+ " idDirector = (" + 
					"select idDirector from"
					+ " stores as st,"
					+ " accounts as ac,"
					+ " manager_detail as md " + 
					" where ac.id = "+ idManager
					+ " and ac.id = md.idManger"
					+ " and md.idStore = st.id" + 
					" group by idDirector" + 
					")");
			ContactEntities contact = new ContactEntities();
			while(data.next()) {
				contact.setId(data.getInt("id")); ;
				contact.setExpiration(data.getDate("expiration"));
				contact.setNote(data.getString("note"));
				contact.setIdAdmin(data.getInt("idAdmin"));
				contact.setIdDirector(data.getInt("idDirector"));
                return true;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public boolean getExpirationForEmployee(int idEmployee) {
		try {
			String sqlDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			ResultSet data = (ResultSet) HandleDB.Query("select * from contacts where expiration > "+ sqlDate
					+ " and idDirector = " + 
					"(select idDirector"
					+ " from stores as st, accounts as ac, employee_detail as ed" + 
					" where ac.id =" + idEmployee
					+ " and ac.id = ed.idEmployee and ed.idStore = st.id" 
					+ " group by idDirector" 
					+")");
			ContactEntities contact = new ContactEntities();
			while(data.next()) {
				contact.setId(data.getInt("id")); ;
				contact.setExpiration(data.getDate("expiration"));
				contact.setNote(data.getString("note"));
				contact.setIdAdmin(data.getInt("idAdmin"));
				contact.setIdDirector(data.getInt("idDirector"));
                return true;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public boolean updateContractExtension(int idContract, Date dateExtension) {
		try {
			String sqlDate = new SimpleDateFormat("yyyy-MM-dd").format(dateExtension);
			String query = "update contacts set"
					+ " expiration = '"+sqlDate+"'" 
					+" where id='"+idContract+"'";
			boolean result = HandleDB.Mutation(query);
			return result;
		}catch(Exception err) {
			return false;
		}
	}

}
