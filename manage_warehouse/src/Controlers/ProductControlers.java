package Controlers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import Entities.ProductCategoriesEntities;
import Entities.ProductEntities;

public class ProductControlers {

	public boolean insertProduct(ProductEntities product) {
		try {
			String sqlDate = new SimpleDateFormat("yyyy-MM-dd").format(product.getExpiration());
			String query = "insert into products(name,description, prices, cost, expiration ) values("
					+ "'"+product.getName()+"'" 
					+",'"+product.getDescription()+"'"
					+","+product.getPrices()+""
					+","+product.getCost()+""
					+",'"+sqlDate+"'"
					+")";
			boolean result = HandleDB.Mutation(query);
			return result;
		}catch(Exception err) {
			return false;
		}
	}
	
	public boolean insertProductDetail(int idProduct, int idProductCategories) {
		try {
			String query = "insert into product_detail(idProductcategories,idProduct ) values("
					+ ""+idProductCategories+"" 
					+","+idProduct+""
					+")";
			boolean result = HandleDB.Mutation(query);
			return result;
		}catch(Exception err) {
			return false;
		}
	}

	public boolean updateProduct(ProductEntities product) {
		try {
			String sqlDate = new SimpleDateFormat("yyyy-MM-dd").format(product.getExpiration());
			String query = "update products set"
					+ " name = '"+product.getName()+"'" 
					+ ", description = '"+product.getDescription()+"'" 
					+ ", prices = "+product.getPrices()+"" 
					+ ", cost = "+product.getCost()+"" 
					+ ", expiration = '"+sqlDate+"'"
					+" where id='"+product.getId()+"'";
			boolean result = HandleDB.Mutation(query);
			return result;
		}catch(Exception err) {
			return false;
		}
	}

	public boolean deleteProduct(int id) {
		try {
			String query = "update products set isDelete = true"
					+" where id='"+id+"'";
			boolean result = HandleDB.Mutation(query);
			return result;
		}catch(Exception err) {
			return false;
		}
	}

	public ProductEntities findOne(ProductEntities product) {
		try {
			String sqlDate = new SimpleDateFormat("yyyy-MM-dd").format(product.getExpiration());
			ResultSet data = (ResultSet) HandleDB.Query("select * from products where isDelete = false "
					+ " and name ='"+product.getName()+ "'"
					+ " and description ='"+product.getDescription()+ "'"
					+ " and prices ="+product.getPrices()+ ""
					+ " and cost ="+product.getCost()+ ""
					+ " and expiration ='"+sqlDate+ "'"
					+ " ORDER BY id DESC");	
			ProductEntities pc = new ProductEntities();
			while(data.next()) {
				pc.setId(data.getInt("id")); 
			}
			return pc;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public boolean handleProduct(int idProductDetail, boolean isDelete) {
		try {
			String query = "update product_detail set isDelete ="
					+ isDelete
					+" where id='"+idProductDetail+"'";
			boolean result = HandleDB.Mutation(query);
			return result;
		}catch(Exception err) {
			return false;
		}
	}

	public List<ProductEntities> getAllProduct(int idDirector) {
		try {
			ResultSet data = (ResultSet) HandleDB.Query(
					"select p.id, p.name, p.description, p.prices, p.cost, st.name as storeName, pc.name as productcategories"
					+ " from"
					+" stores as st inner join product_category_detail as pcDetail inner join product_categories as pc inner join product_detail as pDetail inner join products as p" 
					+" where " 
					+" idDirector =" + idDirector  
					+" and st.id = pcDetail.idStore" 
					+" and pcDetail.idProductCategories = pc.id"
					+" and pc.id = pDetail.idProductCategories"
					+" and pDetail.idProduct = p.id"
					+" group by id, name, description, prices, cost, storeName, productcategories");
			List<ProductEntities> list = new ArrayList<>();
			
			while(data.next()) {
				ProductEntities p = new ProductEntities();
				p.setId(data.getInt("id")); ;
				p.setName(data.getString("name"));
				p.setDescription(data.getString("description"));
				p.setPrices(data.getInt("prices"));
				p.setCost(data.getInt("cost"));
				p.setStoreName(data.getNString("storeName"));
				p.setProductcategories(data.getNString("productcategories"));
                list.add(p);
			}
			return list;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
