package Controlers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import Entities.DirectorEntities;
import Entities.StoreEntities;

public class StoreControlers {

	public boolean insert(StoreEntities store) {
		try {
			String sqlDate = new SimpleDateFormat("yyyy-MM-dd").format(store.getCreateAt());
			String query = "insert into stores(name, address, phone, createAt ) values("
					+ "'"+store.getName()+"'" 
					+",'"+store.getAddress()+"'"
					+","+store.getPhone()+""
					+",'"+sqlDate+"'"
					+")";
			boolean result = HandleDB.Mutation(query);
			return result;
		}catch(Exception err) {
			return false;
		}
	}

	public boolean update(StoreEntities store) {
		try {
			String query = "update stores set"
					+ " name = '"+store.getName()+"'" 
					+ " ,address = '"+store.getAddress()+"'"
					+ " ,phone = "+store.getPhone()+"" 
					+" where id="+store.getId()+"";
			boolean result = HandleDB.Mutation(query);
			return result;
		}catch(Exception err) {
			return false;
		}
	}

	public boolean delete(StoreEntities store) {
		try {
			String query = "update stores set isDelete = true"
					+" where id="+store.getId()+"";
			boolean result = HandleDB.Mutation(query);
			return result;
		}catch(Exception err) {
			return false;
		}
	}

	public boolean findOne(int idStore, int idDirector) {
		ResultSet data;
		try {
			data = (ResultSet) HandleDB.Query("select * from stores where"
					+ " id = "+ idStore
					+ " and idDirector = " + idDirector);
			while(data.next()) {
			}
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

}
