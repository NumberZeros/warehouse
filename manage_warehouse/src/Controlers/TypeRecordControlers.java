package Controlers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Entities.TypeRecordEntities;

public class TypeRecordControlers {
	public List<TypeRecordEntities> getAll()
	{
		try {
			ResultSet data = (ResultSet) HandleDB.Query("select * from type_records ");
			List<TypeRecordEntities> listType = new ArrayList<>();
			while(data.next()) {
				TypeRecordEntities typeRecord = new TypeRecordEntities();
				typeRecord.setId(data.getInt("id"));
				typeRecord.setName(data.getString("name"));
                listType.add(typeRecord);
			}
			return listType;
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public TypeRecordEntities findOne(int id) {
		try {
			ResultSet data = (ResultSet) HandleDB.Query("select * from type_records where id=" + id);
			TypeRecordEntities typeRecord = new TypeRecordEntities();
			while(data.next()) {
				typeRecord.setId(data.getInt("id"));
				typeRecord.setName(data.getString("name"));
			}
			return typeRecord;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
