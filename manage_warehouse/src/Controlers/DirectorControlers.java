package Controlers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import Entities.AccountEntities;
import Entities.DirectorEntities;

public class DirectorControlers {

	public boolean inster(AccountEntities director) {
		try {
			String sqlDate = new SimpleDateFormat("yyyy-MM-dd").format(director.getBirthday());
			String query = "insert into accounts(name,birthday, email, address, phone, gender,idTypeAccount, password ) values("
					+ "'"+director.getName()+"'" 
					+",'"+sqlDate+"'"
					+",'"+director.getEmail()+"'"
					+",'"+director.getAddress()+"'"
					+",'"+director.getPhone()+"'"
					+",'"+director.getGender()+"'"
					+",'"+director.getIdTypeAccount()+"'"
					+",'"+director.getPassword()+"'"
					+")";
			boolean result = HandleDB.Mutation(query);
			return result;
		}catch(Exception err) {
			return false;
		}
	}

	public boolean update(AccountEntities director) {
		try {
			String sqlDate = new SimpleDateFormat("yyyy-MM-dd").format(director.getBirthday());
			String query = "update accounts set "
					+ "name = '"+director.getName()+"'" 
					+", birthday = '"+sqlDate+"'"
					+",email = '"+director.getEmail()+"'"
					+",address = '"+director.getAddress()+"'"
					+",phone = '"+director.getPhone()+"'"
					+",gender = '"+director.getGender()+"'"
					+",idTypeAccount = '"+director.getIdTypeAccount()+"'"
					+",password = '"+director.getPassword()+"'"
					+"where id ='"+director.getId()+"'";
			boolean result = HandleDB.Mutation(query);
			return result;
		}catch(Exception err) {
			return false;
		}
	}

	public boolean delete(AccountEntities director) {
		try {
			String query = "update accounts set "
					+"isDelete = "+true+""
					+" where id ='"+director.getId()+"'";
			boolean result = HandleDB.Mutation(query);
			return result;
		}catch(Exception err) {
			return false;
		}
	}

	public List<DirectorEntities> getAll() {
		try {
			ResultSet data = (ResultSet) HandleDB.Query("select * from accounts where idTypeAccount = (select id from type_accounts where name = 'DIRECTOR') and isDelete = false ");
			List<DirectorEntities> list = new ArrayList<>();
			while(data.next()) {
				DirectorEntities director = new DirectorEntities();
				director.setId(data.getInt("id"));
				director.setName(data.getString("name"));
				director.setEmail(data.getString("email"));
				director.setPassword(data.getString("password"));
				director.setBirthday(data.getDate("birthday"));
				director.setPhone(data.getInt("phone"));
				director.setGender(data.getString("gender"));
				director.setAddress(data.getString("address"));
                list.add(director);
			}
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public DirectorEntities findOne(int idDirect) {
		try {
			ResultSet data = (ResultSet) HandleDB.Query("select * from accounts where idTypeAccount = (select id from type_accounts where name = 'DIRECTOR') and isDelete = false "
					+ "id ="+ idDirect +"");
			DirectorEntities director = new DirectorEntities();
			while(data.next()) {
				director.setId(data.getInt("id"));
				director.setName(data.getString("name"));
				director.setEmail(data.getString("email"));
				director.setPassword(data.getString("password"));
				director.setBirthday(data.getDate("birthday"));
				director.setPhone(data.getInt("phone"));
				director.setGender(data.getString("gender"));
				director.setAddress(data.getString("address"));
			}
			return director;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public List<AccountEntities> getAllManager(int idStore, int idDirector) {
		try {
			ResultSet data = (ResultSet) HandleDB.Query("select "
					+ "ac.id, ac.name, ac.email, ac.password, ac.birthday, ac.phone, ac.gender, ac.address" + 
					" from " + 
					"accounts as ac inner join stores as st " + 
					"where " + 
					"idTypeAccount = (select id from type_accounts where name = 'MANAGER') "
					+ "and ac.isDelete = false "
					+ "and st.id ="+ idStore
					+ " and st.idDirector =" + idDirector
					+" group by id, name, email, password, birthday, phone, gender, address");
			List<AccountEntities> list = new ArrayList<>();
			while(data.next()) {
				AccountEntities manager = new AccountEntities();
				manager.setId(data.getInt("id"));
				manager.setName(data.getString("name"));
				manager.setEmail(data.getString("email"));
				manager.setPassword(data.getString("password"));
				manager.setBirthday(data.getDate("birthday"));
				manager.setPhone(data.getInt("phone"));
				manager.setGender(data.getString("gender"));
				manager.setAddress(data.getString("address"));
                list.add(manager);
			}
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
