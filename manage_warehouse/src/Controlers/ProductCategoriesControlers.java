package Controlers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import Entities.ProductCategoriesEntities;

public class ProductCategoriesControlers {

	public boolean insertPc(ProductCategoriesEntities pc) {
		try {
			String query = "insert into product_categories(name,description) values("
					+ "'"+pc.getName()+"'" 
					+",'"+pc.getDescription()+"'"
					+")";
			boolean result = HandleDB.Mutation(query);
			return result;
		}catch(Exception err) {
			return false;
		}
	}

	public ProductCategoriesEntities findOne(ProductCategoriesEntities pcEntities) {
		try {
			ResultSet data = (ResultSet) HandleDB.Query("select * from product_categories where isDelete = false "
					+ " and name ='"+pcEntities.getName()+ "'"
					+ " and description ='"+pcEntities.getDescription()+ "'"
					+ " ORDER BY id DESC");
					
			ProductCategoriesEntities pc = new ProductCategoriesEntities();
			while(data.next()) {
				pc.setId(data.getInt("id"));
                return pc;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public boolean insertPCDetail(int idProductCategories, int idStore) {
		try {
			String sqlDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			String query = "insert into product_category_detail(idProductcategories,idStore, createAt ) values("
					+ ""+idProductCategories+"" 
					+","+idStore+""
					+",'"+sqlDate+"'"
					+")";
			boolean result = HandleDB.Mutation(query);
			return result;
		}catch(Exception err) {
			return false;
		}
	}

	public ProductCategoriesEntities findOnePCDetail(int idPCDetail) {
		try {
			ResultSet data = (ResultSet) HandleDB.Query("select * from product_category_detail where"
					+ " id ="+idPCDetail+ "");
			ProductCategoriesEntities pc = new ProductCategoriesEntities();
			while(data.next()) {
				pc.setId(data.getInt("idProductCategories"));
			}
			return pc;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public boolean updateProducCategories(String name, String description, int idProductCategories) {
		try {
			String query = "update product_categories set"
					+ " name = '"+name+"'" 
					+ ", description = '"+description+"'" 
					+" where id='"+idProductCategories+"'";
			boolean result = HandleDB.Mutation(query);
			return result;
		}catch(Exception err) {
			return false;
		}
	}

	public boolean deleteProducCategories(int idProductCategories) {
		try {
			String query = "update product_categories set"
					+ " isDelete = true"
					+" where id='"+idProductCategories+"'";
			boolean result = HandleDB.Mutation(query);
			return result;
		}catch(Exception err) {
			return false;
		}
	}

	public List<ProductCategoriesEntities> getAllProductCatetories() {
		try {
			ResultSet data = (ResultSet) HandleDB.Query(
					"select " + 
					"pcDetail.id as idPCDetail, idStore, idProductCategories, name, description " + 
					"from " + 
					"product_category_detail as pcDetail inner join  product_categories as pc" + 
					" where pc.id = pcDetail.idProductCategories" + 
					" group by " + 
					"idPCDetail, idStore, idProductCategories, name, description"
					);
			List<ProductCategoriesEntities> list = new ArrayList<>();
			
			while(data.next()) {
				ProductCategoriesEntities pc = new ProductCategoriesEntities();
				pc.setId(data.getInt("idProductCategories")); ;
				pc.setIdPCDetail(data.getInt("idPCDetail"));
				pc.setIdStore(data.getInt("idStore"));
				pc.setName(data.getString("name"));
				pc.setDescription(data.getNString("description"));
                list.add(pc);
			}
			return list;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public boolean handleProducCategories(int idPCDetail, boolean isDelete) {
		try {
			String query = "update product_categories set"
					+ " isDelete = "+isDelete+""
					+" where id='"+idPCDetail+"'";
			boolean result = HandleDB.Mutation(query);
			return result;
		}catch(Exception err) {
			return false;
		}
	}

}
