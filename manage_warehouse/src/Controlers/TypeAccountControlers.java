package Controlers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Entities.TypeAccountEntities;

public class TypeAccountControlers {

	public List<TypeAccountEntities> getAll() {
		try {
			ResultSet data = (ResultSet) HandleDB.Query("select * from type_accounts ");
			List<TypeAccountEntities> listType = new ArrayList<>();
			while(data.next()) {
				TypeAccountEntities typeAccount = new TypeAccountEntities();
				typeAccount.setId(data.getInt("id"));
				typeAccount.setName(data.getString("name"));
                listType.add(typeAccount);
			}
			return listType;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public TypeAccountEntities findOne(Integer id) {
		try {
			ResultSet data = (ResultSet) HandleDB.Query("select * from type_accounts where id="+"'"+id+"'");
			TypeAccountEntities typeAccount = new TypeAccountEntities();
			while(data.next()) {
				typeAccount.setId(data.getInt("id"));
				typeAccount.setName(data.getString("name").toUpperCase());
			}
			return typeAccount;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public TypeAccountEntities findOne(String name) {
		try {
			ResultSet data = (ResultSet) HandleDB.Query("select * from type_accounts where name="+"'"+name+"'");
			TypeAccountEntities typeAccount = new TypeAccountEntities();
			while(data.next()) {
				typeAccount.setId(data.getInt("id"));
				typeAccount.setName(data.getString("name"));	
			}
			return typeAccount;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
