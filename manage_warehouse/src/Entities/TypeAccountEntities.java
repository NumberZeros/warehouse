package Entities;

public class TypeAccountEntities {
	private int id;
	private String name;
	
	// Enum
	public static String ADMIN = "Admin";
	public static String DIRECTOR = "Directors";
	public static String MANAGER = "Manager";
	public static String EMPLOYEE_IMPORT = "Employee Import";
	public static String EMPLOYEE_ADJUSTMENT = "Employee Adjustment";
	public static String EMPLOYEE_EXPORT = "Employee Export";

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		switch(name) {
		case "ADMIN": 
			this.name = ADMIN;
			break;
		case "DIRECTOR":
			this.name = DIRECTOR;
			break;
		case "MANAGER":
			this.name = MANAGER;
			break;
		case "EMPLOYEE_IMPORT":
			this.name = EMPLOYEE_IMPORT;
			break;
		case "EMPLOYEE_ADJUSTMENT":
			this.name = EMPLOYEE_ADJUSTMENT;
			break;
		case "EMPLOYEE_EXPORT":
			this.name = EMPLOYEE_EXPORT;
			break;
			default: break;
		}
	}
	
	public TypeAccountEntities() {}

}
