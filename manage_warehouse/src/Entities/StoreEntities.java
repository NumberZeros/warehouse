package Entities;

import java.util.Date;

public class StoreEntities {
	private int id;
	private String name;
	private String address;
	private int phone;
	private Date createAt;

	public StoreEntities() {
	};

	public StoreEntities(int id, String name, String address, int phone, Date createAt) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.createAt = createAt;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}
}
