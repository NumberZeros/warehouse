package Entities;

import java.util.Date;

public class RecordEntities {
	private int id;
	private String description;
	private int createBy;
	private Date createAt;//
	private int totalProducts;//
	private float totalPrices;//
	private boolean isVerify;//
	private int verifyBy;//
	private int idStore;
	private int idTypeRecord;
	private boolean isDelete;//
	
	public RecordEntities(int id, String description, int createBy, Date createAt, int totalProducts, float totalPrices,
			boolean isVerify, int verifyBy, int idStore, int idTypeRecord, boolean isDelete) {
		super();
		this.id = id;
		this.description = description;
		this.createBy = createBy;
		this.createAt = createAt;
		this.totalProducts = totalProducts;
		this.totalPrices = totalPrices;
		this.isVerify = isVerify;
		this.verifyBy = verifyBy;
		this.idStore = idStore;
		this.idTypeRecord = idTypeRecord;
		this.isDelete = isDelete;
	}

	public RecordEntities() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getCreateBy() {
		return createBy;
	}

	public void setCreateBy(int createBy) {
		this.createBy = createBy;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public int getTotalProducts() {
		return totalProducts;
	}

	public void setTotalProducts(int totalProducts) {
		this.totalProducts = totalProducts;
	}

	public float getTotalPrices() {
		return totalPrices;
	}

	public void setTotalPrices(float totalPrices) {
		this.totalPrices = totalPrices;
	}

	public boolean isVerify() {
		return isVerify;
	}

	public void setVerify(boolean isVerify) {
		this.isVerify = isVerify;
	}

	public int getVerifyBy() {
		return verifyBy;
	}

	public void setVerifyBy(int verifyBy) {
		this.verifyBy = verifyBy;
	}

	public int getIdStore() {
		return idStore;
	}

	public void setIdStore(int idStore) {
		this.idStore = idStore;
	}

	public int getIdTypeRecord() {
		return idTypeRecord;
	}

	public void setIdTypeRecord(int idTypeRecord) {
		this.idTypeRecord = idTypeRecord;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
}
