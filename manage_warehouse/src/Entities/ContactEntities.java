package Entities;

import java.util.Date;

public class ContactEntities {
	private int id;
	private Date expiration;
	private String note;
	private int idAdmin;
	private int idDirector;

	public ContactEntities() {
	}

	public ContactEntities(int id, Date expiration, String note, int idAdmin, int idDirector, boolean isDelete) {
		super();
		this.id = id;
		this.expiration = expiration;
		this.note = note;
		this.idAdmin = idAdmin;
		this.idDirector = idDirector;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getExpiration() {
		return expiration;
	}

	public void setExpiration(Date expiration) {
		this.expiration = expiration;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public int getIdAdmin() {
		return idAdmin;
	}

	public void setIdAdmin(int idAdmin) {
		this.idAdmin = idAdmin;
	}

	public int getIdDirector() {
		return idDirector;
	}

	public void setIdDirector(int idDirector) {
		this.idDirector = idDirector;
	}
	
	//Method
	
	public void find(int id) {
		this.id = id;
	}

	public void insert( String note, int idAdmin, int idDirector) {
		this.expiration = new Date();
		this.note = note;
		this.idAdmin = idAdmin;
		this.idDirector = idDirector;
	}
	
	public void update(int id,Date expiration, String note, int idAdmin, int idDirector) {
		this.id = id;
		this.expiration = expiration;
		this.note = note;
		this.idAdmin = idAdmin;
		this.idDirector = idDirector;
	}
	
	public void delete(int id) {
		this.id = id;
	}

}
