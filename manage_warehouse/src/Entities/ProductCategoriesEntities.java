package Entities;

public class ProductCategoriesEntities {
	private int id;
	private String name;
	private String description;
	private boolean isDelete;

	private int idPCDetail;
	private int idStore;

	public int getIdStore() {
		return idStore;
	}

	public void setIdStore(int idStore) {
		this.idStore = idStore;
	}

	public int getIdPCDetail() {
		return idPCDetail;
	}

	public void setIdPCDetail(int idPCDetail) {
		this.idPCDetail = idPCDetail;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public ProductCategoriesEntities() {
	};

	public ProductCategoriesEntities(int id, String name, String description, boolean isDelete) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.isDelete = isDelete;
	}

}
