package Entities;

public class TypeRecordEntities {
	private int id;
	private String name;
	
	//enum
	public static String ADJUSTMENT = "Adjustment";
	public static String IMPORT = "Import";
	public static String EXPORT = "Export";
	
	public TypeRecordEntities() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TypeRecordEntities(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		switch (name) {
		case "ADJUSTMENT":
			this.name = "ADJUSTMENT";
			break;
		case "IMPORT":
			this.name = "IMPORT";
			break;
		case "EXPORT":
			this.name = "EXPORT";
			break;
		}
	}
}
