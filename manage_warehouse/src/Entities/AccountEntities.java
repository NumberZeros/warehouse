package Entities;
import java.util.Date;

public class AccountEntities {
	protected int id;
	protected String name;
	protected Date birthday;
	protected String address;
	protected int phone;
	protected String gender;
	protected String email;
	protected String password;
	protected int idTypeAccount;
	protected Date createAt;
	protected boolean isDelete;
	
	//get set
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getPhone() {
		return phone;
	}
	public void setPhone(int phone) {
		this.phone = phone;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getIdTypeAccount() {
		return idTypeAccount;
	}
	public void setIdTypeAccount(int idTypeAccount) {
		this.idTypeAccount = idTypeAccount;
	}
	public Date getCreateAt() {
		return createAt;
	}
	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}
	public boolean getIsDelete() {
		return isDelete;
	}
	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	// Method
	
	public void login(String email, String password) {
		this.email = email;
		this.password = password;
		this.isDelete = false;
	}
	
	public void getInfo(int id) {
		this.id = id;
		this.isDelete = false;
	}
	public AccountEntities() {};
	public AccountEntities(int id, String name, Date birthday, String address, int phone, String gender, String email,
			String password, int idTypeAccount) {
		super();
		this.id = id;
		this.name = name;
		this.birthday = birthday;
		this.address = address;
		this.phone = phone;
		this.gender = gender;
		this.email = email;
		this.password = password;
		this.idTypeAccount = idTypeAccount;
		this.createAt = new Date();
		this.isDelete = false;
	}
	
}
