package Entities;

public class RecordDetailEntities {
	public RecordDetailEntities() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	private int id;
	private int idRecord;
	private int idProduct;
	private int number;
	private float totalAmount;
	
	public RecordDetailEntities(int id, int idRecord, int idProduct, int number, float totalAmount) {
		super();
		this.id = id;
		this.idRecord = idRecord;
		this.idProduct = idProduct;
		this.number = number;
		this.totalAmount = totalAmount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdRecord() {
		return idRecord;
	}

	public void setIdRecord(int idRecord) {
		this.idRecord = idRecord;
	}

	public int getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public float getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}
}
