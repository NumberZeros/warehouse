package Entities;

import java.util.Date;

public class ProductEntities {
	private int id;
	private String name;
	private String description;
	private int prices;
	private int cost;
	private Date expiration;
	private boolean isDelete;
	
	private String storeName;
	private String productcategories;

	public ProductEntities() {
	};

	public ProductEntities(int id, String name, String description, int prices, int cost, Date expiration,
			boolean isDelete) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.prices = prices;
		this.cost = cost;
		this.expiration = expiration;
		this.isDelete = isDelete;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPrices() {
		return prices;
	}

	public void setPrices(int prices) {
		this.prices = prices;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public Date getExpiration() {
		return expiration;
	}

	public void setExpiration(Date expiration) {
		this.expiration = expiration;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getProductcategories() {
		return productcategories;
	}

	public void setProductcategories(String productcategories) {
		this.productcategories = productcategories;
	}
	
}
