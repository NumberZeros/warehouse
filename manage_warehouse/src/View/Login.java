package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Entities.AccountEntities;
import Entities.TypeAccountEntities;
import Models.AccountModels;
import Models.TypeAccountModels;

import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JTree;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.JButton;
import javax.swing.UIManager;
import javax.swing.JPasswordField;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import java.awt.TextField;
public class Login extends JFrame {

	private JPanel contentPane;
	private JTextField tfAccount;
	private AccountModels accountModel = new AccountModels();
	private AccountEntities accountEntity = new AccountEntities();
	private TypeAccountModels typeModel = new TypeAccountModels();
	private TypeAccountEntities typeEntity = new TypeAccountEntities();
	private JTextField tfPassword;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * Validate Email
	 */
	public static boolean isEmailValid(String email) {
	    final Pattern EMAIL_REGEX = Pattern.compile("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", Pattern.CASE_INSENSITIVE);
	    return EMAIL_REGEX.matcher(email).matches();
	}


	public void LoginCheck() {
			if (!isEmailValid(this.tfAccount.getText()) || String.valueOf(this.tfPassword.getText()).isEmpty())
				{				
				JOptionPane.showMessageDialog(null, 
											  "Nhập lại tài khoản/mật khẩu", 
											  "TITLE", 
											  JOptionPane.WARNING_MESSAGE);		
				}
			else {
				//accountEntity = 
				accountEntity = accountModel.handleLogin(this.tfAccount.getText(),this.tfPassword.getText().toString());
				typeEntity = typeModel.findOne(accountEntity.getIdTypeAccount());
				System.out.println(typeEntity.getName());
				
			}
	}

	/**
	 * Create the frame.
	 */
		
	public Login() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(600, 120, 373, 504);
		contentPane = new JPanel();
		contentPane.setForeground(UIManager.getColor("List.selectionBackground"));
		contentPane.setBackground(new Color(204, 153, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnLogin = new JButton("\u0110\u0103ng nh\u1EADp");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				LoginCheck();
			}
		});
		btnLogin.setBackground(UIManager.getColor("InternalFrame.inactiveBorderColor"));
		btnLogin.setIcon(new ImageIcon("E:\\Nam3\\log-in.png"));
		btnLogin.setBounds(179, 294, 133, 46);
		btnLogin.setBorder(null);
		contentPane.add(btnLogin);
		
		JLabel lblAccount = new JLabel("T\u00E0i kho\u1EA3n:");
		lblAccount.setBounds(95, 199, 70, 19);
		contentPane.add(lblAccount);
		
		tfAccount = new JTextField();
		tfAccount.setBounds(95, 218, 154, 19);
		contentPane.add(tfAccount);
		tfAccount.setColumns(10);
		
		JLabel lblPassWord = new JLabel("M\u1EADt kh\u1EA9u:");
		lblPassWord.setBounds(95, 244, 70, 19);
		contentPane.add(lblPassWord);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setIcon(new ImageIcon("E:\\Nam3\\garage (1).png"));
		lblNewLabel.setBounds(95, 45, 159, 144);
		contentPane.add(lblNewLabel);
		
		tfPassword = new JTextField();
		tfPassword.setColumns(10);
		tfPassword.setBounds(95, 262, 154, 19);
		contentPane.add(tfPassword);
	}
}
